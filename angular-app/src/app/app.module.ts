import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { DatePipe } from '@angular/common';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MyarticlesComponent } from './components/myarticles/myarticles.component';
import { NewarticleComponent } from './components/newarticle/newarticle.component';
import { EditarticleComponent} from './components/editarticle/editarticle.component';
import { ArticleComponent } from './components/article/article.component';

import { ValidateService } from './services/validate/validate.service';
import { UserService } from './services/user/user.service';
import { LocalstorageService } from './services/localstorage/localstorage.service';
import { ArticleService } from './services/article/article.service';

import { AuthGuard } from './guards/auth.guards';
import { AuthorGuard } from './guards/authorGuard.guards';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'article', component: ArticleComponent},
  {path: 'myarticles', component: MyarticlesComponent, canActivate: [AuthorGuard]},
  {path: 'newarticle', component: NewarticleComponent, canActivate: [AuthorGuard]},
  {path: 'editarticle', component: EditarticleComponent, canActivate: [AuthorGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    MyarticlesComponent,
    NewarticleComponent,
    EditarticleComponent,
    ArticleComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    TagInputModule,
    BrowserAnimationsModule, 
    ReactiveFormsModule,
  ],
  providers: [
    ValidateService,
    UserService,
    LocalstorageService,
    ArticleService,
    DatePipe,
    AuthGuard,
    AuthorGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
