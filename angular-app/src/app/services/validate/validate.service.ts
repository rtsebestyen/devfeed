import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateRegister(user) {
    if (user.username === undefined || user.email === undefined || user.password === undefined) {
      return false;
    } else {
      return true;
    }
  }

  validateLogin(user) {
    if (user.username === undefined || user.username === "" || user.password === undefined || user.password === "")
      return false;
    else
      return true;
  }

  validateNewArticle(article, list) {
    if (article.title === undefined || 
        article.title === "" || 
        article.description === undefined || 
        article.description === "" ||
        article.link === undefined || 
        article.link === "" ||
        list.length <= 0)
      return false;
    else
      return true;
  }

  validate

  validateEmail(email){
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
  }

}
