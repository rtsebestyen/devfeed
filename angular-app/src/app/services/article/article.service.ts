import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
declare var swal: any;

import { UserService } from '../user/user.service';
import { LocalstorageService } from '../localstorage/localstorage.service';

@Injectable()
export class ArticleService {
  authToken: any;
  user: any;

  constructor(
    private http: Http ,
    private userService: UserService,
    private localStorageService: LocalstorageService
  ) { }

  insertArticle(article) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/insertArticle', article, {headers: headers})
      .map(res => res.json());
  }

  getArticlesByAuthorId(authorId) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/findAllArticlesByAuthorId', authorId, {headers: headers})
      .map(res => res.json());
  }

  deleteArticleById(req) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    headers.append('articleId', req.id);
    return this.http.delete('articles/deleteArticleById', {headers: headers})
      .map(res => res.json());
  }

  editArticleById(article) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/editArticleById', article, {headers: headers})
      .map(res => res.json());
  }

  findAllArticles(limit, page) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('articles/findAllArticles/' + limit + '/' + page, {headers: headers})
      .map(res => res.json());
  }

  // Push votes
  pushVoterToArticle(ids) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/pushVoterToArticle', ids, {headers: headers})
      .map(res => res.json());
  }

  pushDownvoterToArticle(ids) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/pushDownvoterToArticle', ids, {headers: headers})
      .map(res => res.json());
  }

  // Pop votes
  popVoterFromArticle(ids) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/popVoterFromArticle', ids, {headers: headers})
      .map(res => res.json());
  }

  popDownvoterFromArticle(ids) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/popDownvoterFromArticle', ids, {headers: headers})
      .map(res => res.json());
  }


  findIfUserUpvotedArticle(ids) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/findIfUserUpvotedArticle', ids, {headers: headers})
      .map(res => res.json());
  }

  findIfUserDownvotedArticle(ids) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/findIfUserDownvotedArticle', ids, {headers: headers})
      .map(res => res.json());
  }

  findArticleById(articleId) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('articles/findArticleById/' + articleId, {headers: headers})
      .map(res => res.json());
  }

  inserComment(data) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/insertComment', data, {headers: headers})
      .map(res => res.json());
  }

  findAllTags() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('articles/findAllTags', {headers: headers})
      .map(res => res.json());
  }

  filterArticlesByTag(tags) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('articles/filterArticlesByTag', tags, {headers: headers})
      .map(res => res.json());
  }

  uploadPicture(file) {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    return this.http.post('articles/uploadPicture', file, {headers: headers})
      .map(res => res.json());
  }

  loadToken() {
    this.authToken = this.localStorageService.loadToken();
  }

  upvoteArticle(articleId) {
    // create object containing the user and article Id
    this.user = JSON.parse(this.localStorageService.getUser());
    const ids = {
      userId: this.user.id,
      articleId: articleId
    };

    // find if user already upvoted the article
    this.findIfUserUpvotedArticle(ids).subscribe(data => {
      if (data.success) {

        // the user already uvoted
        if (data.article.length === 1) {
          // pop the upvote
          this.popVoterFromArticle(ids).subscribe(data2 => {
            if (data2.success) {
              swal({ title: 'Upvote cleared', type: 'success'});
            } else {
              swal({ title: 'Something went wrong!', type: 'error'});
            }
          });
        } else {

        // check if the user downvoted
        // if yes then pop downvote
        this.findIfUserDownvotedArticle(ids).subscribe(data3 => {
          if (data3.success) {
            if (data3.article.length === 1) {
              this.popDownvoterFromArticle(ids).subscribe(data4 => {
                if (data4.success) {
                }
              });
            }
          } else {
            swal({ title: 'Something went wrong!', type: 'error'});
          }
        });

        // push upvote
        this.pushVoterToArticle(ids).subscribe(data2 => {
            if (data2.success) {
              swal({title: 'Upvoted', type: 'success'});
            } else {
              swal({title: 'Something went wrong!', type: 'error'});
            }
          });
        }
      } else {
        console.log('home component error: findIfUserUpvotedArticle');
        console.log(data.err);
      }
    });
  }

  downVote(articleId) {
    // create object containing the user and article Id
    this.user = JSON.parse(this.localStorageService.getUser());
    const ids = {
      userId: this.user.id,
      articleId: articleId
    };

    // find if user already downvoted the article
    this.findIfUserDownvotedArticle(ids).subscribe(data => {
      if (data.success) {

        // the user already downvoted
        if (data.article.length === 1) {
          // pop the downvote
          this.popDownvoterFromArticle(ids).subscribe(data2 => {
            if (data2.success) {
              swal({ title: 'Downvote cleared', type: 'success'});
            } else {
              swal({ title: 'Something went wrong!', type: 'error'});
            }
          });
        } else {

        // check if the user upvoted
        // if yes then pop upvote
        this.findIfUserUpvotedArticle(ids).subscribe(data3 => {
          if (data3.success) {
            if (data3.article.length === 1) {
              this.popVoterFromArticle(ids).subscribe(data4 => {
                if (data4.success) {
                }
              });
            }
          } else {
            swal({ title: 'Something went wrong!', type: 'error'});
          }
        });

        // push downvote
        this.pushDownvoterToArticle(ids).subscribe(data2 => {
            if (data2.success) {
              swal({title: 'Downvoted', type: 'success'});
            } else {
              swal({title: 'Something went wrong!', type: 'error'});
            }
          });
        }
      } else {
        console.log('home component error: findIfUserUpvotedArticle');
        console.log(data.err);
      }
    });
  }

}
