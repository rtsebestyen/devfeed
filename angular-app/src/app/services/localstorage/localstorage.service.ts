import { Injectable } from '@angular/core';

@Injectable()
export class LocalstorageService {

  constructor() { }

  loadToken() {
    return localStorage.getItem('id_token');
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', user);
  }

  clearLocalStorage() {
    localStorage.clear();
  }

  getUser() {
    // return the user in string format
    return localStorage.getItem('user');
  }

}
