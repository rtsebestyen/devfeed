import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

import { LocalstorageService } from '../localstorage/localstorage.service';

@Injectable()
export class UserService {
  authToken: any;
  user: any;

  constructor(
    private http: Http,
    private localstorageService: LocalstorageService) { }

  registerUser(user) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('users/register', user, {headers: headers})
      .map(res => res.json());
  }

  loginUser(user) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('users/login', user, {headers: headers})
      .map(res => res.json());
  }

  getProfile() {
    const headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('users/profile', {headers: headers})
      .map(res => res.json());
  }

  findUserById(id) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('users/findUserById/' + id, {headers: headers})
      .map(res => res.json());
  }

  loadToken() {
    this.authToken = this.localstorageService.loadToken();
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  storeUserData(token, user) {
    this.localstorageService.storeUserData(token, user);
    this.authToken = token;
    this.user = user;
  }

  isAuthor() {
    const userObj = JSON.parse(this.localstorageService.getUser());
    return userObj.role === 'Author';
  }

  logoutUser() {
    this.authToken = null;
    this.user = null;
    this.localstorageService.clearLocalStorage();
  }

}
