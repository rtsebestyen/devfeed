import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';

import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { ValidateService } from '../../services/validate/validate.service';

declare var swal: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: String;
  password: String;

  constructor(
    private userService: UserService,
    private router: Router,
    private localstorageService: LocalstorageService,
    private validateService: ValidateService
  ) { }

  ngOnInit() {
    this.password = "";
    this.username = "";
  }

  loginUser() {
    const user = {
      username: this.username,
      password: this.password
    };
    if (this.validateService.validateLogin(user)){
      this.userService.loginUser(user).subscribe(data => {
        if (data.success) {
          this.userService.storeUserData(data.token, data.user);
          swal({
            title: 'Succes!',
            text: 'Successfull login!',
            type: 'success'
          });
          this.router.navigate(['/']);
        } else {
          swal({
            title: 'Error!',
            text: 'Wrong username or password!',
            type: 'error'
          });
        }
      });
    } else {
      swal({
        title: 'Error!',
        text: 'You didn\'t enter the username or password!',
        type: 'error'
      });
    }
  }
}
