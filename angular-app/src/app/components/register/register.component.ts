import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate/validate.service';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
declare var swal: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  firstName: String;
  lastName: String;
  username: String;
  password: String;
  email: String;
  roleSelected: String = 'User';
  roles: any = [ 'User', 'Author' ];

  constructor(
    private validateService: ValidateService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  registerUser(event: any) {
    const user = {
      firstName: this.firstName,
      lastName: this.lastName,
      username: this.username,
      password: this .password,
      email: this.email,
      role: this.roleSelected
    };
    // Fields
    if (!this.validateService.validateRegister(user)) {
      swal({
        title: 'ERROR!',
        text: 'Please fill in all the fields!',
        type: 'error'
      });
      return false;
    }
    // Email
    if (!this.validateService.validateEmail(user.email)) {
      swal({
        title: 'ERROR!',
        text: 'Please fill in a valid e-mail!',
        type: 'error'
      });
      return false;
    }
    // Register User
    this.userService.registerUser(user).subscribe(data => {
      if (data.success) {
        swal({
          title: 'Successfull registration',
          text: 'The registration was successfull, you can log in now!',
          type: 'success'
        });
        this.router.navigate(['/login']);
      } else {
        swal({
          title: 'Unsuccessfull registration',
          text: 'The registration was unsuccessfull!',
          type: 'error'
        });
      }
    });
  }

}
