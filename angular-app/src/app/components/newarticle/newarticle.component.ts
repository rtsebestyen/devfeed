import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { ArticleService } from '../../services/article/article.service';
import { ValidateService } from '../../services/validate/validate.service';

declare var swal: any;

@Component({
  selector: 'app-newarticle',
  templateUrl: './newarticle.component.html',
  styleUrls: ['./newarticle.component.css']
})

export class NewarticleComponent implements OnInit {
  title: String;
  description: String;
  link: String;

  tags:any;
  selectedTag: String;
  tagList: Array<Tag> = [];
  file: any;

  constructor(
    private datePipe: DatePipe,
    private localstorageService: LocalstorageService,
    private articleService: ArticleService,
    private router: Router,
    private validateService: ValidateService
  ) { }

  ngOnInit() {
    this.getTags();
  }

  getTags(){
    this.articleService.findAllTags().subscribe(data => {
      if (data.success) {
        this.tags = data.tags;
        this.selectedTag = data.tags[0].name;
      } else {
        console.log("newarticle comp: findalltags: something went wrong")
      }
    });
  }

  onSubmit() {
    const dateNow = Date.now();
    const userObj = JSON.parse(this.localstorageService.getUser());
    const date = this.datePipe.transform(dateNow, 'yyyy/MM/dd');
    const article = {
      authorId: userObj.id,
      authorName: userObj.username,
      title: this.title,
      description: this.description,
      link: this.link,
      date: date.toString(),
      tags: this.tagList
    };

    // validation
    if (this.validateService.validateNewArticle(article, this.tagList)){
      this.articleService.insertArticle(article).subscribe(data => {
        if (data.success) {
          swal({
            title: 'SUCCESS',
            text: 'The article was inserted!',
            type: 'success'
          });
          this.router.navigate(['/myarticles']);
        } else {
          console.log(data.err);
          swal({
            title: 'ERROR',
            text: 'The article was not inserted!',
            type: 'error'
          });
        }
      });
    } else {
      swal({
        title: 'ERROR',
        text: 'There are empty fields!',
        type: 'error'
      });
    }
  }

  addTagToList(){
    let insert = true;
    this.tagList.forEach(element => {
      if (element.name === this.selectedTag){
        insert = false;
      }
    });
    if (insert){
      let insertTag = new Tag();
      insertTag.name = this.selectedTag;
      this.tagList.push(insertTag);
    } else {
      swal({
        title: 'ERROR',
        text: 'Duplicate error',
        type: 'error'
      });
    }
  }


  writeTags(){
    console.log("==============");
    console.log("List contains:");
    this.tagList.forEach(element => {
      console.log("TagName: " + element.name);
    });
    console.log("==============");
  }

  fileChange(event){
    this.file = event.target.files[0];
  }

}

export class Tag
{
  name: String;
}

