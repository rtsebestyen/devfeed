import { Component, OnInit,  ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { UserService } from '../../services/user/user.service';
import { ArticleService } from '../../services/article/article.service';
import { Tag } from '../newarticle/newarticle.component';

declare var swal: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  articles: any;
  user: any;
  tags: any;
  selectedTag: String;
  tagList: Array<Tag> = [];
  page: any;
  limit: number;
  checked: boolean;
  userID: any;
  upvoteName: any;
  downvoteName: any;

  @ViewChild('buttonId') buttonId: ElementRef;

  constructor(
    private localstorageService: LocalstorageService,
    private articleService: ArticleService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.limit = 9;
    this.page = 1;
    this.checked = false;
    this.userID = JSON.parse(this.localstorageService.getUser());
    let user = JSON.parse(this.localstorageService.getUser());
    if (user !== null) {
      this.userID = user.id;
    }
    this.showArticles();
    this.getTags();
  }

  showArticles() {
    this.articleService.findAllArticles(this.limit, this.page).subscribe(data => {
      if (data.success) {
        this.articles = data.articles;
      } else {
        console.log('something went wrong');
      }
    });
  }

  getTags(){
    this.articleService.findAllTags().subscribe(data => {
      if (data.success) {
        this.tags = data.tags;
        this.selectedTag = data.tags[0].name;
      } else {
        console.log("newarticle comp: findalltags: something went wrong")
      }
    });
  }

  upvote(articleId) {
    // the user has to login
    if (!this.userService.loggedIn()) {
      this.router.navigate(['/login']);

    // the user is logged in
    } else {
      this.articleService.upvoteArticle(articleId);
    }
    //this.router.navigate(['/']);
 }

  downvote(articleId) {
    if (!this.userService.loggedIn()) {
      this.router.navigate(['/login']);
    } else {
      this.articleService.downVote(articleId);
    }
  }

  addTagToList(){
    let insert = true;
    this.tagList.forEach(element => {
      if (element.name === this.selectedTag){
        insert = false;
      }
    });
    if (insert){
      let insertTag = new Tag();
      insertTag.name = this.selectedTag;
      this.tagList.push(insertTag);
    } else {
      swal({
        title: 'ERROR',
        text: 'Duplicate error',
        type: 'error'
      });
    }
  }

  filterArticles(){
    const tagJson = {
      tags: [],
      limit: this.limit,
      page: this.page
    }
    for (let i in this.tagList){
      tagJson.tags.push(this.tagList[i].name);
    }
    this.articleService.filterArticlesByTag(tagJson).subscribe(data => {
      if (data.success) {
        this.articles = data.tags;
      } else {
        console.log('something went wrong:' + data.err);
      }
    });
  }

  prevPageState(){
    return this.page === 1;
  }

  getPrevPage(){
    this.page = this.page - 1;
    if (this.checked === false) {
      this.showArticles();
    } else if (this.checked === true) {
      this.filterArticles();
    }
  }

  getNextPage(){
    if (Object.keys(this.articles).length !== 0){
      this.page = this.page + 1;
      if (this.checked === false) {
          this.showArticles();
      } else if (this.checked === true) {
          this.filterArticles();
      }
    }
  }

  changeFilter(){
    if (this.checked === false){
      this.page = 1;
      this.showArticles();
    }
  }

  checkLike(articleId){
    var ret = false;

    //find the article likes
    var likes: any;
    this.articles.forEach(article => {
      if(article._id === articleId){
        likes = article.upvotes;
      }
    });
    likes.forEach(userIds => {
      if (userIds === this.userID){
        ret = true
      }
    });
    return ret;
  }

  checkDislike(articleId){
    var ret = false;

    //find the article dislikes
    var dislikes: any;
    this.articles.forEach(article => {
      if(article._id === articleId){
        dislikes = article.downvotes;
      }
    });
    dislikes.forEach(userIds => {
      if (userIds === this.userID){
        ret = true
      }
    });
    return ret;
  }

}
