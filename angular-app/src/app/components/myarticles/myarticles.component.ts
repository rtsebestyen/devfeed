import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var swal: any;

import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { ArticleService } from '../../services/article/article.service';

@Component({
  selector: 'app-myarticles',
  templateUrl: './myarticles.component.html',
  styleUrls: ['./myarticles.component.css']
})
export class MyarticlesComponent implements OnInit {
  articles: any;
  articleToEdit: any;

  constructor(
    private localstorageService: LocalstorageService,
    private articleService: ArticleService,
    private router: Router
  ) { }

  ngOnInit() {
    this.fillTableWithArticles();
  }

  fillTableWithArticles() {
    const userObj = JSON.parse(localStorage.getItem('user'));
    const req = {
      id: userObj.id
    };
    this.articleService.getArticlesByAuthorId(req).subscribe(data => {
      if (data.success) {
        this.articles = data.articles;
      } else {
        console.log('something went wrong');
      }
    });
  }

  deleteArticle(article) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this article!",
      type: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        const req = {
          id: article._id
        };
        this.articleService.deleteArticleById(req).subscribe(data => {
          if (data.success) {
            swal({
              title: 'SUCCESS',
              text: 'Article deleted successfully',
              type: 'success'
            });
            var position = 1;
            var found = false;
            this.articles.forEach(element => {
              if(!found){
                if (element._id !== article._id){
                  position = position + 1;
                } else {
                  found = true;
                }
              }
            });
            this.articles.splice(position-1, 1);
                  } else {
            swal({
              title: 'ERROR',
              text: 'The article was not deleted!',
              type: 'error'
            });
          }
        });
      } 
    });
  }

}
