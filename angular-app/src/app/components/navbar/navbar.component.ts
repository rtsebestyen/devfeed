import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onLogoutClick() {
    this.userService.logoutUser();
    swal({
      title: 'Successfull logout!',
      text: 'You are now logged out!',
      type: 'success'
    });
    this.router.navigate(['']);
    return false;
  }

}
