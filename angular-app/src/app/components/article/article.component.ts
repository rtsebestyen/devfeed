import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { ArticleService } from '../../services/article/article.service';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { UserService } from '../../services/user/user.service';

declare var swal: any;

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  title: String;
  description: String;
  link: String;
  articleId: any;
  authorName: any;
  tags: any;
  likes: any;
  dislikes: any;
  userID: any;

  commentmodel: String;
  comments: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleService,
    private router: Router,
    private localStorageService: LocalstorageService,
    private datePipe: DatePipe,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.articleId = this.activatedRoute.snapshot.params['articleId'];
    this.commentmodel = "";
    this.userID = JSON.parse(this.localStorageService.getUser());
    let user = JSON.parse(this.localStorageService.getUser());
    if (user !== null) {
      this.userID = user.id;
    }
    this.findArticleById();
  }

  submitComment(){
    if (this.commentmodel !== ""){
      const dateNow = Date.now();
      const userObj = JSON.parse(this.localStorageService.getUser());
      const date = this.datePipe.transform(dateNow, 'yyyy/MM/dd');
      if (userObj !== null){
      const req = {
        userId: userObj.id,
        username: userObj.username,
        articleId: this.articleId,
        comment: this.commentmodel,
        date: date.toString()
      };
      this.comments = req;
      this.articleService.inserComment(req).subscribe(data => {
        if (data.success) {
          swal({
            title: 'SUCCESS',
            text: 'Comment inserted!',
            type: 'success'
          });
          this.comments = req;
        } else {
          swal({
            title: 'ERROR',
            text: 'Comment was not insterted!',
            type: 'error'
          });
        }
      });
      } else {
        swal({
          title: 'ERROR',
          text: 'You\'re not logged in!',
          type: 'error'
        });
      }
    } else {
      swal({
        title: 'ERROR',
        text: 'The input is empty!',
        type: 'error'
      });
    }
  }

  findArticleById(){
    this.articleService.findArticleById(this.articleId).subscribe(data => {
      if (data.success) {
        this.comments = data.article[0].comments;
        this.title = data.article[0].title;
        this.description = data.article[0].description;
        this.authorName = data.article[0].authorName;
        this.link = data.article[0].link;
        this.tags = data.article[0].tags;
        this.likes = data.article[0].upvotes;
        this.dislikes = data.article[0].downvotes;
      } else {
        console.log("Something went wrong!");
      }
    });
  }

  upvote(articleId) {
    // the user has to login
    if (!this.userService.loggedIn()) {
      this.router.navigate(['/login']);
    // the user is logged in
    } else {
      this.articleService.upvoteArticle(articleId);
    }
 }

  downvote(articleId) {
    if (!this.userService.loggedIn()) {
      this.router.navigate(['/login']);
    } else {
      this.articleService.downVote(articleId);
    }
  }

  checkLike(){
    var ret = false;
    this.likes.forEach(userIds => {
      if (userIds === this.userID){
        ret = true
      }
    });
    return ret;
  }

  checkDislike(){
    var ret = false;
    this.dislikes.forEach(userIds => {
      if (userIds === this.userID){
        ret = true
      }
    });
    return ret;
  }

}
