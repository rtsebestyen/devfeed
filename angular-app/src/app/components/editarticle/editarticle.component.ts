import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import { Tag } from '../newarticle/newarticle.component';

import { ArticleService } from '../../services/article/article.service';

declare var swal: any;

@Component({
  selector: 'app-editarticle',
  templateUrl: './editarticle.component.html',
  styleUrls: ['./editarticle.component.css']
})

export class EditarticleComponent implements OnInit {
  article: any;
  title: String;
  description: String;
  link: String;
  id: any;

  tagList: Array<String> = [];
  selectedTag: String;
  allTags: any;
  articleTags: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleService,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getTags();
    this.getArticleData();
  }

  getArticleData(){
    this.articleService.findArticleById(this.id).subscribe(data => {
      if (data.success){
        this.title = data.article[0].title;
        this.description = data.article[0].description;
        this.link = data.article[0].link;
        this.articleTags = data.article[0].tags;

        for(let i of this.articleTags){
          this.tagList.push(i);
        }
        console.log("edit article tags: " + this.tagList);
      } else {
        console.log("editarticle: findArticleById: something went wrong");
      }
    });
  }

  getTags(){
    this.articleService.findAllTags().subscribe(data => {
      if (data.success) {
        this.allTags = data.tags;
        this.selectedTag =  this.allTags[0].name;
      } else {
        console.log("newarticle comp: findalltags: something went wrong")
      }
    });
  }

  onSubmit() {
    const req = {
      id: this.id,
      title: this.title,
      description: this.description,
      link: this.link,
      tags: this.tagList
    };
    console.log(req);
    this.articleService.editArticleById(req).subscribe(data => {
      if (data.success) {
        swal({
          title: 'SUCCESS',
          text: 'Article updated successfully',
          type: 'success'
        });
        this.router.navigate(['/myarticles']);
      } else {
        console.log("edit article edit error: " + data.error.err);
        swal({
          title: 'ERROR',
          text: 'The article was not updated!',
          type: 'error'
        });
      }
    });
  }

  addTagToList(){
    let insert = true;
    this.tagList.forEach(element => {
      if (element === this.selectedTag){
        insert = false;
      }
    });
    if (insert){
      this.tagList.push(this.selectedTag);
    } else {
      swal({
        title: 'ERROR',
        text: 'Duplicate error',
        type: 'error'
      });
    }
  }

}
