import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../services/user/user.service';
import { LocalstorageService } from '../services/localstorage/localstorage.service';

@Injectable()
export class AuthorGuard implements CanActivate {
    constructor(
        private userService: UserService,
        private router: Router,
        private localStorage: LocalstorageService
    ) {}

    canActivate() {
        const user = JSON.parse(this.localStorage.getUser());
        if (this.userService.loggedIn()) {
            if (user.role === 'Author') {
                return true;
            } else {
                this.router.navigate(['/']);
                return false;
            }
        } else {
            this.router.navigate(['/']);
            return false;
        }
    }
}
