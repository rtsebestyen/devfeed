webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\r\n<div class=\"container\">\r\n    <router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_froala_editor_js_froala_editor_pkgd_min_js__ = __webpack_require__("./node_modules/froala-editor/js/froala_editor.pkgd.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_froala_editor_js_froala_editor_pkgd_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_froala_editor_js_froala_editor_pkgd_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_froala_wysiwyg__ = __webpack_require__("./node_modules/angular-froala-wysiwyg/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_chips__ = __webpack_require__("./node_modules/ngx-chips/esm5/ngx-chips.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_navbar_navbar_component__ = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_login_login_component__ = __webpack_require__("./src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_register_register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_home_home_component__ = __webpack_require__("./src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_profile_profile_component__ = __webpack_require__("./src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_myarticles_myarticles_component__ = __webpack_require__("./src/app/components/myarticles/myarticles.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_newarticle_newarticle_component__ = __webpack_require__("./src/app/components/newarticle/newarticle.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_editarticle_editarticle_component__ = __webpack_require__("./src/app/components/editarticle/editarticle.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_article_article_component__ = __webpack_require__("./src/app/components/article/article.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_validate_validate_service__ = __webpack_require__("./src/app/services/validate/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_article_article_service__ = __webpack_require__("./src/app/services/article/article.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__guards_auth_guards__ = __webpack_require__("./src/app/guards/auth.guards.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__guards_authorGuard_guards__ = __webpack_require__("./src/app/guards/authorGuard.guards.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_14__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_12__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_13__components_register_register_component__["a" /* RegisterComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_15__components_profile_profile_component__["a" /* ProfileComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_24__guards_auth_guards__["a" /* AuthGuard */]] },
    { path: 'article', component: __WEBPACK_IMPORTED_MODULE_19__components_article_article_component__["a" /* ArticleComponent */] },
    { path: 'myarticles', component: __WEBPACK_IMPORTED_MODULE_16__components_myarticles_myarticles_component__["a" /* MyarticlesComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_25__guards_authorGuard_guards__["a" /* AuthorGuard */]] },
    { path: 'newarticle', component: __WEBPACK_IMPORTED_MODULE_17__components_newarticle_newarticle_component__["a" /* NewarticleComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_25__guards_authorGuard_guards__["a" /* AuthorGuard */]] },
    { path: 'editarticle', component: __WEBPACK_IMPORTED_MODULE_18__components_editarticle_editarticle_component__["a" /* EditarticleComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_25__guards_authorGuard_guards__["a" /* AuthorGuard */]] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_myarticles_myarticles_component__["a" /* MyarticlesComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_newarticle_newarticle_component__["a" /* NewarticleComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_editarticle_editarticle_component__["a" /* EditarticleComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_article_article_component__["a" /* ArticleComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_6_angular_froala_wysiwyg__["a" /* FroalaEditorModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_angular_froala_wysiwyg__["b" /* FroalaViewModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_ngx_chips__["a" /* TagInputModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* ReactiveFormsModule */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_20__services_validate_validate_service__["a" /* ValidateService */],
                __WEBPACK_IMPORTED_MODULE_21__services_user_user_service__["a" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_22__services_localstorage_localstorage_service__["a" /* LocalstorageService */],
                __WEBPACK_IMPORTED_MODULE_23__services_article_article_service__["a" /* ArticleService */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common__["d" /* DatePipe */],
                __WEBPACK_IMPORTED_MODULE_24__guards_auth_guards__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_25__guards_authorGuard_guards__["a" /* AuthorGuard */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/article/article.component.css":
/***/ (function(module, exports) {

module.exports = ".top-gap{\r\n    margin-top:20px;\r\n}"

/***/ }),

/***/ "./src/app/components/article/article.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"containter text-center top-gap\">\r\n  <div class=\"col-lg-8 mx-auto\">\r\n    <h3>{{title}}</h3>\r\n    <p>By : {{authorName}}</p>\r\n    <p>Description: {{description}}</p>\r\n    <p>Tags: <span class=\"badge badge-primary\" *ngFor=\"let t of tags\">{{t&nbsp;}}</span></p>\r\n    <a [href]=\"link\" class=\"btn btn-info\">Read article</a>\r\n    <button type=\"button\" class=\"btn btn-success\" *ngIf=\"checkLike(articleId)\" (click)=\"upvote(articleId)\" >Upvoted</button>\r\n    <button type=\"button\" class=\"btn btn-success\" *ngIf=\"!checkLike(articleId)\" (click)=\"upvote(articleId)\" >Upvote</button>\r\n    <button type=\"button\" class=\"btn btn-danger\"  *ngIf=\"checkDislike(articleId)\" (click)=\"downvote(articleId)\">Downvoted</button>\r\n    <button type=\"button\" class=\"btn btn-danger\"  *ngIf=\"!checkDislike(articleId)\" (click)=\"downvote(articleId)\">Downvote</button>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-lg-10 mx-auto\">\r\n\r\n  <h4 class=\"top-gap\">Write a comment:</h4>\r\n  \r\n  <form (submit)=\"submitComment()\">\r\n    <div class=\"form-group\">\r\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"commentmodel\" name=\"commentmodel\" placeholder=\"Comment\">\r\n    </div>\r\n    <button type=\"submit\" class=\"btn btn-dark\">Submit</button>\r\n  </form>\r\n\r\n  <div class=\"top-gap\">\r\n    <h4>Comments:</h4>\r\n  </div>\r\n\r\n  <div class=\"card\" *ngFor=\"let c of comments\">\r\n    <div class=\"card-header\">\r\n      {{c.username}}\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <p class=\"card-text\">{{c.comment}}</p>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/article/article.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArticleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_article_article_service__ = __webpack_require__("./src/app/services/article/article.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ArticleComponent = /** @class */ (function () {
    function ArticleComponent(activatedRoute, articleService, router, localStorageService, datePipe, userService) {
        this.activatedRoute = activatedRoute;
        this.articleService = articleService;
        this.router = router;
        this.localStorageService = localStorageService;
        this.datePipe = datePipe;
        this.userService = userService;
    }
    ArticleComponent.prototype.ngOnInit = function () {
        this.articleId = this.activatedRoute.snapshot.params['articleId'];
        this.commentmodel = "";
        this.userID = JSON.parse(this.localStorageService.getUser());
        var user = JSON.parse(this.localStorageService.getUser());
        if (user !== null) {
            this.userID = user.id;
        }
        this.findArticleById();
    };
    ArticleComponent.prototype.submitComment = function () {
        var _this = this;
        if (this.commentmodel !== "") {
            var dateNow = Date.now();
            var userObj = JSON.parse(this.localStorageService.getUser());
            var date = this.datePipe.transform(dateNow, 'yyyy/MM/dd');
            if (userObj !== null) {
                var req_1 = {
                    userId: userObj.id,
                    username: userObj.username,
                    articleId: this.articleId,
                    comment: this.commentmodel,
                    date: date.toString()
                };
                this.comments = req_1;
                this.articleService.inserComment(req_1).subscribe(function (data) {
                    if (data.success) {
                        swal({
                            title: 'SUCCESS',
                            text: 'Comment inserted!',
                            type: 'success'
                        });
                        _this.comments = req_1;
                    }
                    else {
                        swal({
                            title: 'ERROR',
                            text: 'Comment was not insterted!',
                            type: 'error'
                        });
                    }
                });
            }
            else {
                swal({
                    title: 'ERROR',
                    text: 'You\'re not logged in!',
                    type: 'error'
                });
            }
        }
        else {
            swal({
                title: 'ERROR',
                text: 'The input is empty!',
                type: 'error'
            });
        }
    };
    ArticleComponent.prototype.findArticleById = function () {
        var _this = this;
        this.articleService.findArticleById(this.articleId).subscribe(function (data) {
            if (data.success) {
                _this.comments = data.article[0].comments;
                _this.title = data.article[0].title;
                _this.description = data.article[0].description;
                _this.authorName = data.article[0].authorName;
                _this.link = data.article[0].link;
                _this.tags = data.article[0].tags;
                _this.likes = data.article[0].upvotes;
                _this.dislikes = data.article[0].downvotes;
            }
            else {
                console.log("Something went wrong!");
            }
        });
    };
    ArticleComponent.prototype.upvote = function (articleId) {
        // the user has to login
        if (!this.userService.loggedIn()) {
            this.router.navigate(['/login']);
            // the user is logged in
        }
        else {
            this.articleService.upvoteArticle(articleId);
        }
    };
    ArticleComponent.prototype.downvote = function (articleId) {
        if (!this.userService.loggedIn()) {
            this.router.navigate(['/login']);
        }
        else {
            this.articleService.downVote(articleId);
        }
    };
    ArticleComponent.prototype.checkLike = function () {
        var _this = this;
        var ret = false;
        this.likes.forEach(function (userIds) {
            if (userIds === _this.userID) {
                ret = true;
            }
        });
        return ret;
    };
    ArticleComponent.prototype.checkDislike = function () {
        var _this = this;
        var ret = false;
        this.dislikes.forEach(function (userIds) {
            if (userIds === _this.userID) {
                ret = true;
            }
        });
        return ret;
    };
    ArticleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-article',
            template: __webpack_require__("./src/app/components/article/article.component.html"),
            styles: [__webpack_require__("./src/app/components/article/article.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_article_article_service__["a" /* ArticleService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__services_localstorage_localstorage_service__["a" /* LocalstorageService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_5__services_user_user_service__["a" /* UserService */]])
    ], ArticleComponent);
    return ArticleComponent;
}());



/***/ }),

/***/ "./src/app/components/editarticle/editarticle.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/editarticle/editarticle.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <h1 class=\"display-4\">Edit article</h1>\r\n  <form (submit)=\"onSubmit()\">\r\n      <div class=\"form-group\">\r\n          <label>Title</label>\r\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"title\" name=\"title\" placeholder=\"Title\">\r\n      </div>\r\n\r\n      <div class=\"form-group\">\r\n          <label>Description</label>\r\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"description\" name=\"description\" placeholder=\"Description\">\r\n      </div>\r\n      \r\n      <div class=\"form-group\">\r\n          <label>Link</label>\r\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"link\" name=\"link\" placeholder=\"Link to the article\">\r\n      </div>\r\n\r\n      <div class=\"form-group\">\r\n        <form class=\"form-inline\">\r\n            <div class=\"form-group mb-3\">\r\n                <label>Tags: </label>\r\n            </div>\r\n            <div class=\"form-group mx-sm-2 mb-2\">\r\n                <select class=\"custom-select\" [(ngModel)]=\"selectedTag\" name=\"selectedTag\">\r\n                    <option *ngFor=\"let t of allTags\" [ngValue]=\"t.name\">{{t.name}}</option>\r\n                </select>\r\n            </div>\r\n            <button class=\"btn btn-primary\" (click)=\"addTagToList()\">Add</button>\r\n        </form>\r\n        <tag-input [(ngModel)]=\"tagList\" \r\n                    name=\"tagList\" \r\n                    [identifyBy]=\"'name'\" \r\n                    [displayBy]=\"'name'\" \r\n                    [allowDupes]='false'\r\n                    [editable]='false'\r\n                    [modelAsStrings]='true'\r\n                    [hideForm]='true'\r\n                    theme=\"bootstrap\"\r\n                    >\r\n        </tag-input>\r\n    </div>\r\n\r\n      <button type=\"submit\" class=\"btn btn-dark\">Submit</button>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/editarticle/editarticle.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditarticleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_article_article_service__ = __webpack_require__("./src/app/services/article/article.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditarticleComponent = /** @class */ (function () {
    function EditarticleComponent(activatedRoute, articleService, router) {
        this.activatedRoute = activatedRoute;
        this.articleService = articleService;
        this.router = router;
        this.tagList = [];
    }
    EditarticleComponent.prototype.ngOnInit = function () {
        this.id = this.activatedRoute.snapshot.params['id'];
        this.getTags();
        this.getArticleData();
    };
    EditarticleComponent.prototype.getArticleData = function () {
        var _this = this;
        this.articleService.findArticleById(this.id).subscribe(function (data) {
            if (data.success) {
                _this.title = data.article[0].title;
                _this.description = data.article[0].description;
                _this.link = data.article[0].link;
                _this.articleTags = data.article[0].tags;
                for (var _i = 0, _a = _this.articleTags; _i < _a.length; _i++) {
                    var i = _a[_i];
                    _this.tagList.push(i);
                }
                console.log("edit article tags: " + _this.tagList);
            }
            else {
                console.log("editarticle: findArticleById: something went wrong");
            }
        });
    };
    EditarticleComponent.prototype.getTags = function () {
        var _this = this;
        this.articleService.findAllTags().subscribe(function (data) {
            if (data.success) {
                _this.allTags = data.tags;
                _this.selectedTag = _this.allTags[0].name;
            }
            else {
                console.log("newarticle comp: findalltags: something went wrong");
            }
        });
    };
    EditarticleComponent.prototype.onSubmit = function () {
        var _this = this;
        var req = {
            id: this.id,
            title: this.title,
            description: this.description,
            link: this.link,
            tags: this.tagList
        };
        console.log(req);
        this.articleService.editArticleById(req).subscribe(function (data) {
            if (data.success) {
                swal({
                    title: 'SUCCESS',
                    text: 'Article updated successfully',
                    type: 'success'
                });
                _this.router.navigate(['/myarticles']);
            }
            else {
                console.log("edit article edit error: " + data.error.err);
                swal({
                    title: 'ERROR',
                    text: 'The article was not updated!',
                    type: 'error'
                });
            }
        });
    };
    EditarticleComponent.prototype.addTagToList = function () {
        var _this = this;
        var insert = true;
        this.tagList.forEach(function (element) {
            if (element === _this.selectedTag) {
                insert = false;
            }
        });
        if (insert) {
            this.tagList.push(this.selectedTag);
        }
        else {
            swal({
                title: 'ERROR',
                text: 'Duplicate error',
                type: 'error'
            });
        }
    };
    EditarticleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-editarticle',
            template: __webpack_require__("./src/app/components/editarticle/editarticle.component.html"),
            styles: [__webpack_require__("./src/app/components/editarticle/editarticle.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__services_article_article_service__["a" /* ArticleService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], EditarticleComponent);
    return EditarticleComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/***/ (function(module, exports) {

module.exports = "table { \r\n    table-layout: fixed; \r\n}\r\n \r\ntable th, table td { \r\n    overflow: hidden; \r\n}\r\n \r\n#cardId {\r\n    width:75%;\r\n    margin: 0 auto;\r\n    margin-top: 25px;\r\n}\r\n \r\n#header {\r\n    text-align: center;\r\n}\r\n \r\n#buttonId {\r\n    color: white;\r\n    background: #2C4A52;\r\n}\r\n  "

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"display-4\" id=\"header\" style=\"color: #2C4A52; margin-top: 2%\">Articles</h1>\r\n\r\n\r\n<form class=\"form-inline\" id=\"cardId\">\r\n  <div class=\"form-check mr-3\">\r\n      <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"checked\" name=\"filter\" (change)=\"changeFilter()\">\r\n      <label class=\"form-check-label\" for=\"checkbox11\" style=\"color: #2C4A52; size: 10px\">Filter</label>\r\n  </div>\r\n</form>\r\n\r\n<div *ngIf=\"checked\" id=\"cardId\">\r\n  <div class=\"form-group\">\r\n    <form class=\"form-inline\">\r\n      <div class=\"form-group mb-3\">\r\n          <label>Tags: </label>\r\n      </div>\r\n      <div class=\"form-group mx-sm-2 mb-2\">\r\n          <select class=\"custom-select\" [(ngModel)]=\"selectedTag\" name=\"selectedTag\">\r\n              <option *ngFor=\"let t of tags\" [ngValue]=\"t.name\">{{t.name}}</option>\r\n          </select>\r\n      </div>\r\n      <button class=\"btn btn-success\" (click)=\"addTagToList()\">Add</button>\r\n  </form>\r\n  <tag-input [(ngModel)]=\"tagList\" \r\n              name=\"tagList\" \r\n              [identifyBy]=\"'name'\" \r\n              [displayBy]=\"'name'\" \r\n              [allowDupes]='false'\r\n              [editable]='false'\r\n              [modelAsStrings]='true'\r\n              [hideForm]='true'\r\n              theme=\"bootstrap\"\r\n              >\r\n  </tag-input>\r\n  <button class=\"btn btn-success\" (click)=\"filterArticles()\">Filter</button>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"card\" id=\"cardId\" *ngFor=\"let a of articles\" style=\"border: 1px solid #2C4A52;\">\r\n  <h5 [routerLink]=\"['/article',{articleId:a._id}]\" class=\"card-header\" >{{a.title}}</h5>\r\n  <div class=\"card-body\">\r\n    <h5 class=\"card-title\">By: {{a.authorName}}</h5>\r\n    <p class=\"card-text\">{{a.description}}</p>\r\n    <p>Tags: <span class=\"badge badge-primary\" *ngFor=\"let t of a.tags\">{{t&nbsp;}}</span></p>\r\n    \r\n    <ul class=\"list-group list-group-flush\" >\r\n      <li class=\"list-group-item center\" >\r\n        <a [href]=\"a.link\" class=\"btn btn-info\">Read article</a>\r\n        <button type=\"button\" class=\"btn btn-success\" *ngIf=\"checkLike(a._id)\" (click)=\"upvote(a._id)\" >Upvoted</button>\r\n        <button type=\"button\" class=\"btn btn-success\" *ngIf=\"!checkLike(a._id)\" (click)=\"upvote(a._id)\" >Upvote</button>\r\n        <button type=\"button\" class=\"btn btn-danger\"  *ngIf=\"checkDislike(a._id)\" (click)=\"downvote(a._id)\">Downvoted</button>\r\n        <button type=\"button\" class=\"btn btn-danger\"  *ngIf=\"!checkDislike(a._id)\" (click)=\"downvote(a._id)\">Downvote</button>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>\r\n\r\n<div id=\"cardId\">\r\n  <button class=\"btn btn-secondary\" id=\"buttonId\" (click)=\"getPrevPage()\" [disabled]=\"prevPageState()\"><<</button>\r\n  <button class=\"btn btn-secondary\" id=\"buttonId\" (click)=\"getNextPage()\">>></button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_article_article_service__ = __webpack_require__("./src/app/services/article/article.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__newarticle_newarticle_component__ = __webpack_require__("./src/app/components/newarticle/newarticle.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomeComponent = /** @class */ (function () {
    function HomeComponent(localstorageService, articleService, userService, router) {
        this.localstorageService = localstorageService;
        this.articleService = articleService;
        this.userService = userService;
        this.router = router;
        this.tagList = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.limit = 9;
        this.page = 1;
        this.checked = false;
        this.userID = JSON.parse(this.localstorageService.getUser());
        var user = JSON.parse(this.localstorageService.getUser());
        if (user !== null) {
            this.userID = user.id;
        }
        this.showArticles();
        this.getTags();
    };
    HomeComponent.prototype.showArticles = function () {
        var _this = this;
        this.articleService.findAllArticles(this.limit, this.page).subscribe(function (data) {
            if (data.success) {
                _this.articles = data.articles;
            }
            else {
                console.log('something went wrong');
            }
        });
    };
    HomeComponent.prototype.getTags = function () {
        var _this = this;
        this.articleService.findAllTags().subscribe(function (data) {
            if (data.success) {
                _this.tags = data.tags;
                _this.selectedTag = data.tags[0].name;
            }
            else {
                console.log("newarticle comp: findalltags: something went wrong");
            }
        });
    };
    HomeComponent.prototype.upvote = function (articleId) {
        // the user has to login
        if (!this.userService.loggedIn()) {
            this.router.navigate(['/login']);
            // the user is logged in
        }
        else {
            this.articleService.upvoteArticle(articleId);
        }
        //this.router.navigate(['/']);
    };
    HomeComponent.prototype.downvote = function (articleId) {
        if (!this.userService.loggedIn()) {
            this.router.navigate(['/login']);
        }
        else {
            this.articleService.downVote(articleId);
        }
    };
    HomeComponent.prototype.addTagToList = function () {
        var _this = this;
        var insert = true;
        this.tagList.forEach(function (element) {
            if (element.name === _this.selectedTag) {
                insert = false;
            }
        });
        if (insert) {
            var insertTag = new __WEBPACK_IMPORTED_MODULE_5__newarticle_newarticle_component__["b" /* Tag */]();
            insertTag.name = this.selectedTag;
            this.tagList.push(insertTag);
        }
        else {
            swal({
                title: 'ERROR',
                text: 'Duplicate error',
                type: 'error'
            });
        }
    };
    HomeComponent.prototype.filterArticles = function () {
        var _this = this;
        var tagJson = {
            tags: [],
            limit: this.limit,
            page: this.page
        };
        for (var i in this.tagList) {
            tagJson.tags.push(this.tagList[i].name);
        }
        this.articleService.filterArticlesByTag(tagJson).subscribe(function (data) {
            if (data.success) {
                _this.articles = data.tags;
            }
            else {
                console.log('something went wrong:' + data.err);
            }
        });
    };
    HomeComponent.prototype.prevPageState = function () {
        return this.page === 1;
    };
    HomeComponent.prototype.getPrevPage = function () {
        this.page = this.page - 1;
        if (this.checked === false) {
            this.showArticles();
        }
        else if (this.checked === true) {
            this.filterArticles();
        }
    };
    HomeComponent.prototype.getNextPage = function () {
        if (Object.keys(this.articles).length !== 0) {
            this.page = this.page + 1;
            if (this.checked === false) {
                this.showArticles();
            }
            else if (this.checked === true) {
                this.filterArticles();
            }
        }
    };
    HomeComponent.prototype.changeFilter = function () {
        if (this.checked === false) {
            this.page = 1;
            this.showArticles();
        }
    };
    HomeComponent.prototype.checkLike = function (articleId) {
        var _this = this;
        var ret = false;
        //find the article likes
        var likes;
        this.articles.forEach(function (article) {
            if (article._id === articleId) {
                likes = article.upvotes;
            }
        });
        likes.forEach(function (userIds) {
            if (userIds === _this.userID) {
                ret = true;
            }
        });
        return ret;
    };
    HomeComponent.prototype.checkDislike = function (articleId) {
        var _this = this;
        var ret = false;
        //find the article dislikes
        var dislikes;
        this.articles.forEach(function (article) {
            if (article._id === articleId) {
                dislikes = article.downvotes;
            }
        });
        dislikes.forEach(function (userIds) {
            if (userIds === _this.userID) {
                ret = true;
            }
        });
        return ret;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('buttonId'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], HomeComponent.prototype, "buttonId", void 0);
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("./src/app/components/home/home.component.html"),
            styles: [__webpack_require__("./src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_localstorage_localstorage_service__["a" /* LocalstorageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_article_article_service__["a" /* ArticleService */],
            __WEBPACK_IMPORTED_MODULE_3__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".top-gap{\r\n    margin-top:20px;\r\n}\r\n\r\n#buttonId {\r\n    color: white;\r\n    background: #2C4A52;\r\n}"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"containter text-center top-gap\">\r\n  <div class=\"col-lg-7 mx-auto\">\r\n    <h2 class=\"page-header\" style=\"color: #2C4A52; margin-top: 2%\">Login</h2>\r\n    <form (submit)=\"loginUser()\">\r\n\r\n      <div class=\"form-group\">\r\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" name=\"username\" placeholder=\"Username\">\r\n      </div>\r\n\r\n      <div class=\"form-group\">\r\n        <input type=\"password\" class=\"form-control\" [(ngModel)]=\"password\" name=\"password\" placeholder=\"Password\">\r\n      </div>\r\n\r\n      <input type=\"submit\" class=\"btn\" id=\"buttonId\" value=\"Login\">\r\n    </form>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_validate_validate_service__ = __webpack_require__("./src/app/services/validate/validate.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(userService, router, localstorageService, validateService) {
        this.userService = userService;
        this.router = router;
        this.localstorageService = localstorageService;
        this.validateService = validateService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.password = "";
        this.username = "";
    };
    LoginComponent.prototype.loginUser = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        if (this.validateService.validateLogin(user)) {
            this.userService.loginUser(user).subscribe(function (data) {
                if (data.success) {
                    _this.userService.storeUserData(data.token, data.user);
                    swal({
                        title: 'Succes!',
                        text: 'Successfull login!',
                        type: 'success'
                    });
                    _this.router.navigate(['/']);
                }
                else {
                    swal({
                        title: 'Error!',
                        text: 'Wrong username or password!',
                        type: 'error'
                    });
                }
            });
        }
        else {
            swal({
                title: 'Error!',
                text: 'You didn\'t enter the username or password!',
                type: 'error'
            });
        }
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/components/login/login.component.html"),
            styles: [__webpack_require__("./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__services_localstorage_localstorage_service__["a" /* LocalstorageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_validate_validate_service__["a" /* ValidateService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/myarticles/myarticles.component.css":
/***/ (function(module, exports) {

module.exports = ".row{\r\n    margin-top: 10px;\r\n    margin-bottom: 10px\r\n}\r\n\r\n.row-m-t{\r\n    margin-top : 10px\r\n}\r\n\r\ntable { \r\n    table-layout: fixed; \r\n}\r\n\r\ntable th, table td { \r\n    overflow: hidden; \r\n}\r\n"

/***/ }),

/***/ "./src/app/components/myarticles/myarticles.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"display-4\" style=\"color: #2C4A52; margin-top: 2%\">My Articles</h1>\r\n\r\n<div class=\"row row-m-t\"></div>\r\n\r\n<a class=\"btn btn-warning btn-lg\" [routerLink]=\"['/newarticle']\"> + New Article</a>\r\n\r\n<div class=\"row row-m-t\"></div>\r\n\r\n<h3 class=\"display-6\">Articles</h3>\r\n\r\n<div class=\"row row-m-t\"></div>\r\n\r\n<table class=\"table table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <th style=\"width: 20%\">Title</th>\r\n            <th style=\"width: 55%\">Description</th>\r\n            <th style=\"width: 10%\">Link</th>\r\n            <th style=\"width: 15%\">Action</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr *ngFor=\"let a of articles\">\r\n            <th [routerLink]=\"['/article',{articleId:a._id}]\">{{a.title}}</th>\r\n            <th>{{a.description}}</th>\r\n            <th>\r\n                <a class=\"btn btn-info\" [href]=\"a.link\">Read</a>\r\n            </th>\r\n            <th>\r\n                <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteArticle(a)\">Delete</button>\r\n                <a class=\"btn btn-primary\" [routerLink]=\"['/editarticle',{id:a._id}]\">Edit</a>\r\n            </th>\r\n        </tr>\r\n    </tbody>\r\n</table>"

/***/ }),

/***/ "./src/app/components/myarticles/myarticles.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyarticlesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_article_article_service__ = __webpack_require__("./src/app/services/article/article.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyarticlesComponent = /** @class */ (function () {
    function MyarticlesComponent(localstorageService, articleService, router) {
        this.localstorageService = localstorageService;
        this.articleService = articleService;
        this.router = router;
    }
    MyarticlesComponent.prototype.ngOnInit = function () {
        this.fillTableWithArticles();
    };
    MyarticlesComponent.prototype.fillTableWithArticles = function () {
        var _this = this;
        var userObj = JSON.parse(localStorage.getItem('user'));
        var req = {
            id: userObj.id
        };
        this.articleService.getArticlesByAuthorId(req).subscribe(function (data) {
            if (data.success) {
                _this.articles = data.articles;
            }
            else {
                console.log('something went wrong');
            }
        });
    };
    MyarticlesComponent.prototype.deleteArticle = function (article) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this article!",
            type: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(function (willDelete) {
            if (willDelete) {
                var req = {
                    id: article._id
                };
                _this.articleService.deleteArticleById(req).subscribe(function (data) {
                    if (data.success) {
                        swal({
                            title: 'SUCCESS',
                            text: 'Article deleted successfully',
                            type: 'success'
                        });
                        var position = 1;
                        var found = false;
                        _this.articles.forEach(function (element) {
                            if (!found) {
                                if (element._id !== article._id) {
                                    position = position + 1;
                                }
                                else {
                                    found = true;
                                }
                            }
                        });
                        _this.articles.splice(position - 1, 1);
                    }
                    else {
                        swal({
                            title: 'ERROR',
                            text: 'The article was not deleted!',
                            type: 'error'
                        });
                    }
                });
            }
        });
    };
    MyarticlesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-myarticles',
            template: __webpack_require__("./src/app/components/myarticles/myarticles.component.html"),
            styles: [__webpack_require__("./src/app/components/myarticles/myarticles.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_localstorage_localstorage_service__["a" /* LocalstorageService */],
            __WEBPACK_IMPORTED_MODULE_3__services_article_article_service__["a" /* ArticleService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], MyarticlesComponent);
    return MyarticlesComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand navbar-dark\" style=\"background-color: #2C4A52;\">\r\n  <a class=\"navbar-brand\" [routerLink]=\"['/']\">DevFeed</a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample02\" aria-controls=\"navbarsExample02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\r\n        <a class=\"nav-link\" [routerLink]=\"['/']\">Home </a>\r\n      </li>\r\n    </ul>\r\n    <ul class=\"navbar-nav ml-auto\">\r\n        <li *ngIf=\"userService.loggedIn() && userService.isAuthor()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\r\n            <a class=\"nav-link\" [routerLink]=\"['/myarticles']\">My Articles </a>\r\n          </li>\r\n        <li *ngIf=\"userService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/profile']\">Profile </a>\r\n        </li>\r\n        <li *ngIf=\"!userService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/login']\">Login </a>\r\n        </li>\r\n        <li *ngIf=\"!userService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/register']\">Register</a>\r\n        </li>\r\n        <li *ngIf=\"userService.loggedIn()\" class=\"nav-item\">\r\n          <a class=\"nav-link\" (click)=\"onLogoutClick()\" href=\"#\">Logout</a>\r\n        </li>\r\n      </ul>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.userService.logoutUser();
        swal({
            title: 'Successfull logout!',
            text: 'You are now logged out!',
            type: 'success'
        });
        this.router.navigate(['']);
        return false;
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("./src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/newarticle/newarticle.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/newarticle/newarticle.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <h1 class=\"display-4\">New article</h1>\r\n    <form (submit)=\"onSubmit()\">\r\n        <div class=\"form-group\">\r\n            <label>Title</label>\r\n            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"title\" name=\"title\" placeholder=\"Title\">\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <label>Description</label>\r\n            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"description\" name=\"description\" placeholder=\"Description\">\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <label>Link</label>\r\n            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"link\" name=\"link\" placeholder=\"Link to the article\">\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <form class=\"form-inline\">\r\n                <div class=\"form-group mb-3\">\r\n                    <label>Tags: </label>\r\n                </div>\r\n                <div class=\"form-group mx-sm-2 mb-2\">\r\n                    <select class=\"custom-select\" [(ngModel)]=\"selectedTag\" name=\"selectedTag\">\r\n                        <option *ngFor=\"let t of tags\" [ngValue]=\"t.name\">{{t.name}}</option>\r\n                    </select>\r\n                </div>\r\n                <button class=\"btn btn-primary\" (click)=\"addTagToList()\">Add</button>\r\n            </form>\r\n            <tag-input [(ngModel)]=\"tagList\" \r\n                        name=\"tagList\" \r\n                        [identifyBy]=\"'name'\" \r\n                        [displayBy]=\"'name'\" \r\n                        [allowDupes]='false'\r\n                        [editable]='false'\r\n                        [modelAsStrings]='true'\r\n                        [hideForm]='true'\r\n                        theme=\"bootstrap\"\r\n                        >\r\n            </tag-input>\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-dark\">Submit</button>\r\n    </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/newarticle/newarticle.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewarticleComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Tag; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_article_article_service__ = __webpack_require__("./src/app/services/article/article.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_validate_validate_service__ = __webpack_require__("./src/app/services/validate/validate.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NewarticleComponent = /** @class */ (function () {
    function NewarticleComponent(datePipe, localstorageService, articleService, router, validateService) {
        this.datePipe = datePipe;
        this.localstorageService = localstorageService;
        this.articleService = articleService;
        this.router = router;
        this.validateService = validateService;
        this.tagList = [];
    }
    NewarticleComponent.prototype.ngOnInit = function () {
        this.getTags();
    };
    NewarticleComponent.prototype.getTags = function () {
        var _this = this;
        this.articleService.findAllTags().subscribe(function (data) {
            if (data.success) {
                _this.tags = data.tags;
                _this.selectedTag = data.tags[0].name;
            }
            else {
                console.log("newarticle comp: findalltags: something went wrong");
            }
        });
    };
    NewarticleComponent.prototype.onSubmit = function () {
        var _this = this;
        var dateNow = Date.now();
        var userObj = JSON.parse(this.localstorageService.getUser());
        var date = this.datePipe.transform(dateNow, 'yyyy/MM/dd');
        var article = {
            authorId: userObj.id,
            authorName: userObj.username,
            title: this.title,
            description: this.description,
            link: this.link,
            date: date.toString(),
            tags: this.tagList
        };
        // validation
        if (this.validateService.validateNewArticle(article, this.tagList)) {
            this.articleService.insertArticle(article).subscribe(function (data) {
                if (data.success) {
                    swal({
                        title: 'SUCCESS',
                        text: 'The article was inserted!',
                        type: 'success'
                    });
                    _this.router.navigate(['/myarticles']);
                }
                else {
                    console.log(data.err);
                    swal({
                        title: 'ERROR',
                        text: 'The article was not inserted!',
                        type: 'error'
                    });
                }
            });
        }
        else {
            swal({
                title: 'ERROR',
                text: 'There are empty fields!',
                type: 'error'
            });
        }
    };
    NewarticleComponent.prototype.addTagToList = function () {
        var _this = this;
        var insert = true;
        this.tagList.forEach(function (element) {
            if (element.name === _this.selectedTag) {
                insert = false;
            }
        });
        if (insert) {
            var insertTag = new Tag();
            insertTag.name = this.selectedTag;
            this.tagList.push(insertTag);
        }
        else {
            swal({
                title: 'ERROR',
                text: 'Duplicate error',
                type: 'error'
            });
        }
    };
    NewarticleComponent.prototype.writeTags = function () {
        console.log("==============");
        console.log("List contains:");
        this.tagList.forEach(function (element) {
            console.log("TagName: " + element.name);
        });
        console.log("==============");
    };
    NewarticleComponent.prototype.fileChange = function (event) {
        this.file = event.target.files[0];
    };
    NewarticleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-newarticle',
            template: __webpack_require__("./src/app/components/newarticle/newarticle.component.html"),
            styles: [__webpack_require__("./src/app/components/newarticle/newarticle.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_3__services_localstorage_localstorage_service__["a" /* LocalstorageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_article_article_service__["a" /* ArticleService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__services_validate_validate_service__["a" /* ValidateService */]])
    ], NewarticleComponent);
    return NewarticleComponent;
}());

var Tag = /** @class */ (function () {
    function Tag() {
    }
    return Tag;
}());



/***/ }),

/***/ "./src/app/components/profile/profile.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\">\r\n  <h1 class=\"display-4\" style=\"color: #2C4A52; margin-top: 2%\">Profile</h1>\r\n\r\n  <label>First Name: {{user.firstName}}</label>\r\n  <br>\r\n  <label>Last Name: {{user.lastName}}</label>\r\n  <br>\r\n  <label>Username: {{user.username}}</label>\r\n  <br>\r\n  <label>Email: {{user.email}}</label>\r\n</div>"

/***/ }),

/***/ "./src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getProfile().subscribe(function (profile) {
            _this.user = profile.user;
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("./src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__("./src/app/components/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/***/ (function(module, exports) {

module.exports = "#buttonId {\r\n    color: white;\r\n    background: #2C4A52;\r\n}"

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\" style=\"color: #2C4A52; margin-top: 2%\">Register</h2>\r\n<form (submit)=\"registerUser($event)\" style=\"margin-top: 1%\">\r\n  <div class=\"form-row\">\r\n    <div class=\"form-group col-md-6\">\r\n      <label>First Name</label>\r\n      <input type=\"text\" class=\"form-control\" [(ngModel)]=\"firstName\" name=\"firstName\" placeholder=\"First Name\">\r\n    </div>\r\n    <div class=\"form-group col-md-6\">\r\n      <label>Last Name</label>\r\n      <input type=\"text\" class=\"form-control\" [(ngModel)]=\"lastName\" name=\"lastName\" placeholder=\"Last Name\">\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"form-group\">\r\n    <label>Username</label>\r\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" name=\"username\" placeholder=\"Username\">\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Email</label>\r\n    <input type=\"email\" class=\"form-control\" [(ngModel)]=\"email\" name=\"email\" placeholder=\"Email\">\r\n  </div>\r\n\r\n  <div class=\"form-row\">\r\n    <div class=\"form-group col-md-6\">\r\n      <label>Password</label>\r\n      <input type=\"password\" class=\"form-control\" [(ngModel)]=\"password\" name=\"password\" placeholder=\"Password\">\r\n    </div>\r\n    <div class=\"form-group col-md-6\">\r\n      <label>Repeat Password</label>\r\n      <input type=\"password\" class=\"form-control\" placeholder=\"Repeat Password\">\r\n    </div>\r\n  </div>  \r\n\r\n  <div class=\"form-row\">\r\n      <div class=\"form-group\">\r\n      <label>Role</label>\r\n      <select class=\"custom-select\" [(ngModel)]=\"roleSelected\" name=\"roleSelected\">\r\n        <option *ngFor=\"let r of roles\" [ngValue]=\"r\">{{r}}</option>\r\n      </select>\r\n      </div>\r\n  </div> \r\n\r\n  <input type=\"submit\" class=\"btn\" id=\"buttonId\" value=\"Register\">\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_validate_service__ = __webpack_require__("./src/app/services/validate/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(validateService, userService, router) {
        this.validateService = validateService;
        this.userService = userService;
        this.router = router;
        this.roleSelected = 'User';
        this.roles = ['User', 'Author'];
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.registerUser = function (event) {
        var _this = this;
        var user = {
            firstName: this.firstName,
            lastName: this.lastName,
            username: this.username,
            password: this.password,
            email: this.email,
            role: this.roleSelected
        };
        // Fields
        if (!this.validateService.validateRegister(user)) {
            swal({
                title: 'ERROR!',
                text: 'Please fill in all the fields!',
                type: 'error'
            });
            return false;
        }
        // Email
        if (!this.validateService.validateEmail(user.email)) {
            swal({
                title: 'ERROR!',
                text: 'Please fill in a valid e-mail!',
                type: 'error'
            });
            return false;
        }
        // Register User
        this.userService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                swal({
                    title: 'Successfull registration',
                    text: 'The registration was successfull, you can log in now!',
                    type: 'success'
                });
                _this.router.navigate(['/login']);
            }
            else {
                swal({
                    title: 'Unsuccessfull registration',
                    text: 'The registration was unsuccessfull!',
                    type: 'error'
                });
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("./src/app/components/register/register.component.html"),
            styles: [__webpack_require__("./src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_validate_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_2__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guards.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (!this.userService.loggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/guards/authorGuard.guards.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthorGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthorGuard = /** @class */ (function () {
    function AuthorGuard(userService, router, localStorage) {
        this.userService = userService;
        this.router = router;
        this.localStorage = localStorage;
    }
    AuthorGuard.prototype.canActivate = function () {
        var user = JSON.parse(this.localStorage.getUser());
        if (this.userService.loggedIn()) {
            if (user.role === 'Author') {
                return true;
            }
            else {
                this.router.navigate(['/']);
                return false;
            }
        }
        else {
            this.router.navigate(['/']);
            return false;
        }
    };
    AuthorGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__services_localstorage_localstorage_service__["a" /* LocalstorageService */]])
    ], AuthorGuard);
    return AuthorGuard;
}());



/***/ }),

/***/ "./src/app/services/article/article.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArticleService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_user_service__ = __webpack_require__("./src/app/services/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ArticleService = /** @class */ (function () {
    function ArticleService(http, userService, localStorageService) {
        this.http = http;
        this.userService = userService;
        this.localStorageService = localStorageService;
    }
    ArticleService.prototype.insertArticle = function (article) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/insertArticle', article, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.getArticlesByAuthorId = function (authorId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/findAllArticlesByAuthorId', authorId, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.deleteArticleById = function (req) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        headers.append('articleId', req.id);
        return this.http.delete('articles/deleteArticleById', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.editArticleById = function (article) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/editArticleById', article, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.findAllArticles = function (limit, page) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.get('articles/findAllArticles/' + limit + '/' + page, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Push votes
    ArticleService.prototype.pushVoterToArticle = function (ids) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/pushVoterToArticle', ids, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.pushDownvoterToArticle = function (ids) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/pushDownvoterToArticle', ids, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Pop votes
    ArticleService.prototype.popVoterFromArticle = function (ids) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/popVoterFromArticle', ids, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.popDownvoterFromArticle = function (ids) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/popDownvoterFromArticle', ids, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.findIfUserUpvotedArticle = function (ids) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/findIfUserUpvotedArticle', ids, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.findIfUserDownvotedArticle = function (ids) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/findIfUserDownvotedArticle', ids, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.findArticleById = function (articleId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.get('articles/findArticleById/' + articleId, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.inserComment = function (data) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/insertComment', data, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.findAllTags = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.get('articles/findAllTags', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.filterArticlesByTag = function (tags) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('articles/filterArticlesByTag', tags, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.uploadPicture = function (file) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.post('articles/uploadPicture', file, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ArticleService.prototype.loadToken = function () {
        this.authToken = this.localStorageService.loadToken();
    };
    ArticleService.prototype.upvoteArticle = function (articleId) {
        var _this = this;
        // create object containing the user and article Id
        this.user = JSON.parse(this.localStorageService.getUser());
        var ids = {
            userId: this.user.id,
            articleId: articleId
        };
        // find if user already upvoted the article
        this.findIfUserUpvotedArticle(ids).subscribe(function (data) {
            if (data.success) {
                // the user already uvoted
                if (data.article.length === 1) {
                    // pop the upvote
                    _this.popVoterFromArticle(ids).subscribe(function (data2) {
                        if (data2.success) {
                            swal({ title: 'Upvote cleared', type: 'success' });
                        }
                        else {
                            swal({ title: 'Something went wrong!', type: 'error' });
                        }
                    });
                }
                else {
                    // check if the user downvoted
                    // if yes then pop downvote
                    _this.findIfUserDownvotedArticle(ids).subscribe(function (data3) {
                        if (data3.success) {
                            if (data3.article.length === 1) {
                                _this.popDownvoterFromArticle(ids).subscribe(function (data4) {
                                    if (data4.success) {
                                    }
                                });
                            }
                        }
                        else {
                            swal({ title: 'Something went wrong!', type: 'error' });
                        }
                    });
                    // push upvote
                    _this.pushVoterToArticle(ids).subscribe(function (data2) {
                        if (data2.success) {
                            swal({ title: 'Upvoted', type: 'success' });
                        }
                        else {
                            swal({ title: 'Something went wrong!', type: 'error' });
                        }
                    });
                }
            }
            else {
                console.log('home component error: findIfUserUpvotedArticle');
                console.log(data.err);
            }
        });
    };
    ArticleService.prototype.downVote = function (articleId) {
        var _this = this;
        // create object containing the user and article Id
        this.user = JSON.parse(this.localStorageService.getUser());
        var ids = {
            userId: this.user.id,
            articleId: articleId
        };
        // find if user already downvoted the article
        this.findIfUserDownvotedArticle(ids).subscribe(function (data) {
            if (data.success) {
                // the user already downvoted
                if (data.article.length === 1) {
                    // pop the downvote
                    _this.popDownvoterFromArticle(ids).subscribe(function (data2) {
                        if (data2.success) {
                            swal({ title: 'Downvote cleared', type: 'success' });
                        }
                        else {
                            swal({ title: 'Something went wrong!', type: 'error' });
                        }
                    });
                }
                else {
                    // check if the user upvoted
                    // if yes then pop upvote
                    _this.findIfUserUpvotedArticle(ids).subscribe(function (data3) {
                        if (data3.success) {
                            if (data3.article.length === 1) {
                                _this.popVoterFromArticle(ids).subscribe(function (data4) {
                                    if (data4.success) {
                                    }
                                });
                            }
                        }
                        else {
                            swal({ title: 'Something went wrong!', type: 'error' });
                        }
                    });
                    // push downvote
                    _this.pushDownvoterToArticle(ids).subscribe(function (data2) {
                        if (data2.success) {
                            swal({ title: 'Downvoted', type: 'success' });
                        }
                        else {
                            swal({ title: 'Something went wrong!', type: 'error' });
                        }
                    });
                }
            }
            else {
                console.log('home component error: findIfUserUpvotedArticle');
                console.log(data.err);
            }
        });
    };
    ArticleService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_4__localstorage_localstorage_service__["a" /* LocalstorageService */]])
    ], ArticleService);
    return ArticleService;
}());



/***/ }),

/***/ "./src/app/services/localstorage/localstorage.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalstorageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.loadToken = function () {
        return localStorage.getItem('id_token');
    };
    LocalstorageService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', user);
    };
    LocalstorageService.prototype.clearLocalStorage = function () {
        localStorage.clear();
    };
    LocalstorageService.prototype.getUser = function () {
        // return the user in string format
        return localStorage.getItem('user');
    };
    LocalstorageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ }),

/***/ "./src/app/services/user/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__("./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__localstorage_localstorage_service__ = __webpack_require__("./src/app/services/localstorage/localstorage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserService = /** @class */ (function () {
    function UserService(http, localstorageService) {
        this.http = http;
        this.localstorageService = localstorageService;
    }
    UserService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('users/register', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    UserService.prototype.loginUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('users/login', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    UserService.prototype.getProfile = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('users/profile', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    UserService.prototype.findUserById = function (id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.get('users/findUserById/' + id, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    UserService.prototype.loadToken = function () {
        this.authToken = this.localstorageService.loadToken();
    };
    UserService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["tokenNotExpired"])('id_token');
    };
    UserService.prototype.storeUserData = function (token, user) {
        this.localstorageService.storeUserData(token, user);
        this.authToken = token;
        this.user = user;
    };
    UserService.prototype.isAuthor = function () {
        var userObj = JSON.parse(this.localstorageService.getUser());
        return userObj.role === 'Author';
    };
    UserService.prototype.logoutUser = function () {
        this.authToken = null;
        this.user = null;
        this.localstorageService.clearLocalStorage();
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_4__localstorage_localstorage_service__["a" /* LocalstorageService */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/services/validate/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidateService = /** @class */ (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegister = function (user) {
        if (user.username === undefined || user.email === undefined || user.password === undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateLogin = function (user) {
        if (user.username === undefined || user.username === "" || user.password === undefined || user.password === "")
            return false;
        else
            return true;
    };
    ValidateService.prototype.validateNewArticle = function (article, list) {
        if (article.title === undefined ||
            article.title === "" ||
            article.description === undefined ||
            article.description === "" ||
            article.link === undefined ||
            article.link === "" ||
            list.length <= 0)
            return false;
        else
            return true;
    };
    ValidateService.prototype.validateEmail = function (email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    };
    ValidateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery__ = __webpack_require__("./node_modules/jquery/dist/jquery.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jquery__);





window['$'] = __WEBPACK_IMPORTED_MODULE_4_jquery__;
window['jQuery'] = __WEBPACK_IMPORTED_MODULE_4_jquery__;
if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map