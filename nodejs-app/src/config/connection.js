const mongoose   = require('mongoose');

function databaseSetup(){
    mongoose.connect(this.database);
    // Connection check, on connect
    mongoose.connection.on('connected', () => {
        console.log('Connected to database: ' + this.database);
    });
    // On error
    mongoose.connection.on('error', (err) => {
        console.log('ERROR on connection: ' + err);
    });
}

module.exports = {
    //database: 'mongodb://localhost:27017/devfeed',
    database: 'mongodb://roberts:feeddev098@ds125272.mlab.com:25272/devfeed',
    secret: 'xrs2b7rsbb897b4gf8b44',
    databaseSetup
}
