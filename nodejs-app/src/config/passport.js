const JwtStrategy   = require('passport-jwt').Strategy,
    ExtractJwt      = require('passport-jwt').ExtractJwt;

const User      = require('../models/userModel'),
    connection  = require('../config/connection');
    userService = require('../services/userService');

module.exports = function(passport){
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt'); // Passing token in the header
    opts.secretOrKey = connection.secret;
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        userService.getUserbyId(jwt_payload.data._id, (err, user) => {
            if (err) {
                return done(err, false);
            }
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        })
    }));
}