const express   = require('express'),
    dateformat  = require('dateformat'),
    passport    = require('passport'),
    router      = express.Router();

const Article      = require('../models/articleModel'),
    Tag            = require('../models/tagModel'),
    articleService = require('../services/articleService');


router.post('/insertArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;

    const newArticle = new Article({
        authorId: body.authorId,
        authorName: body.authorName,
        title: body.title,
        description: body.description,
        link: body.link,
        date: dateformat(body.date, "yyyy/mm/dd").toString(),
        tags: []
    });

    for(var i in body.tags){
        newArticle.tags.push(body.tags[i].name);
    }
    
    articleService.insertArticle(newArticle, (err, article) => {
        if(err){
            res.json({success: false, err: err});
        } else {
            res.json({success: true});
        }
    });
});

router.post('/findAllArticlesByAuthorId', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    articleService.findArticleByAuthorId(body.id, (err, articles) => {
        if(err){
            res.json({success: false});
        } else {
            res.json({success: true, articles: articles});
        }
    });
});

router.delete('/deleteArticleById', passport.authenticate('jwt', {session: false}), (req, res) => {
    const id = req.get('articleId');
    articleService.removeArticleById(id, (err, _) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true});
        }
    });
});

router.post('/editArticleById', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const article = {
        id: body.id,
        title: body.title,
        description: body.description,
        link: body.link,
        tags: body.tags
    };

    articleService.editArticleById(article, (err, _) => {
        if (err) {
            console.log("error: " + err);
            res.json({success: false, error: err});
        } else {
            res.json({success: true});
        }
    });
});

router.get('/findAllArticles/:limit/:page', (req, res) => {
    const limit = req.params.limit;
    const page = req.params.page;
    articleService.findAllArticles(limit, page, (err, articles) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true, articles: articles});
        }
    });
});

router.post('/pushVoterToArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const ids = {
        userId: body.userId,
        articleId: body.articleId
    };
    articleService.pushVoterToArticle(ids, (err, _) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true});
        }
    });
});

router.post('/pushDownvoterToArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const ids = {
        userId: body.userId,
        articleId: body.articleId
    };
    articleService.pushDownvoterToArticle(ids, (err, _) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true});
        }
    });
});

router.post('/popVoterFromArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const ids = {
        userId: body.userId,
        articleId: body.articleId
    };
    articleService.popVoterFromArticle(ids, (err, _) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true});
        }
    });
});

router.post('/popDownvoterFromArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const ids = {
        userId: body.userId,
        articleId: body.articleId
    };
    articleService.popDownvoterFromArticle(ids, (err, _) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true});
        }
    });
});

router.post('/findIfUserUpvotedArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const ids = {
        userId: body.userId,
        articleId: body.articleId
    };
    articleService.findIfUserUpvotedArticle(ids, (err, article) => {
        if (err) {
            res.json({success: false, err: err});
        } else {
            res.json({success: true, article: article});
        }
    });
});

router.post('/findIfUserDownvotedArticle', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const ids = {
        userId: body.userId,
        articleId: body.articleId
    };
    articleService.findIfUserDownvotedArticle(ids, (err, article) => {
        if (err) {
            res.json({success: false, err: err});
        } else {
            res.json({success: true, article: article});
        }
    });
});

router.post('/insertComment', passport.authenticate('jwt', {session: false}), (req, res) => {
    const body = req.body;
    const data = {
        articleId: body.articleId,
        comment: {
            comment: body.comment,
            userId: body.userId,
            username: body.username,
            date: dateformat(body.date, "yyyy/mm/dd").toString() 
        }
    };
    articleService.insertComment(data, (err, article) => {
        if (err) {
            res.json({success: false, err: err});
        } else {
            res.json({success: true, article: article});
        }
    });
});

router.get('/findArticleById/:articleId', (req, res) => {
    articleService.findArticleById(req.params.articleId, (err, article) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true, article: article});
        }
    })
});

router.get('/findAllTags', (_, res) => {
    articleService.findAllTags((err, tags) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true, tags: tags});
        }
    })
});

router.post('/insertTag/:name', (req, res) => {
    const newTag = new Tag({
        name: req.params.name
    });
    articleService.insertTag(newTag, (err, tags) => {
        if (err) {
            res.json({success: false});
        } else {
            res.json({success: true, tags: tags});
        }
    })
});

router.post('/filterArticlesByTag', (req, res) => {
    const tags = req.body.tags;
    const limit = req.body.limit;
    const page = req.body.page;
    articleService.filterArticlesByTag(tags, limit, page, (err, tags) => {
        if (err) {
            res.json({success: false, err: err});
            console.log("filter error: " + err);
        } else {
            res.json({success: true, tags: tags});
        }
    })
});

module.exports = router;