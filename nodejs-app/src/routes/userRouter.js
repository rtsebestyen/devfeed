const express   = require('express'),
    passport    = require('passport'),
    jwt         = require('jsonwebtoken'),
    router      = express.Router();

const User      = require('../models/userModel'),
    connection  = require('../config/connection'),
    userService = require('../services/userService');


router.post('/register', (req, res) => {
    const body = req.body;
    let newUser = new User({ 
        firstName: body.firstName,
        lastName: body.lastName,
        username: body.username,
        email: body.email,
        password: body.password,
        role: body.role
    });

    userService.insertUser(newUser, (err, user) => {
        if(err){
            res.json({success: false, message: 'ERROR: failed to register user: ' + err});
        } else {
            res.json({success: true, message: 'User registered succesfully!'})
        }
    });
});


router.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    
    userService.findUserByUsername(username, (err, user) => {
        if (err) 
            throw err; 
        if (!user)
            return res.json({success: false, message: "Wrong username!"});

        userService.comparePassword(password, user.password, (err, result) => {
            if (err)
                throw err;
            if (result){
                const token = jwt.sign({data: user}, connection.secret, { expiresIn: 86400 });
                const userObj = {
                    id: user.id,
                    username: user.username,
                    role: user.role
                }
                res.json({success: true, token: 'JWT ' + token, user: JSON.stringify(userObj)});
            } else 
                res.json({success: false, message: "Wrong password!"});
        });
    });
});


router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res) => {
    res.json({user: req.user});
});


router.get('/findUserById/:userId', (req, res) => {
    userService.findUserById(req.params.userId, (err, user) => {
        if(err){
            res.json({success: false});
        } else {
            res.json({success: true, user: user});
        }
    });
});

module.exports = router;