const Tag = require('../models/tagModel');

module.exports.findAllTags = function(callback){
    Tag.find(callback);
}

module.exports.insertTag = function(newTag, callback){
    newTag.save(callback);
}