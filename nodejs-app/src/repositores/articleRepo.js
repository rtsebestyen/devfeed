const Article = require('../models/articleModel');


module.exports.save = function(newArticle, callback) { 
    newArticle.save(callback);
}

module.exports.findArticleByAuthorId = function(id, callback) {
    Article.find({authorId: id}).sort({date: -1}).exec(callback);
}

module.exports.removeArticleById = function(id, callback) {
    Article.findByIdAndRemove(id, callback);
}

module.exports.editArticleById = function(article, callback) {
    Article.findByIdAndUpdate(article.id, article, {new: true}, callback);
}

module.exports.findAllArticles = function(limit, skip, callback){
    Article.find().sort({date: -1}).skip(Number(skip)).limit(Number(limit)).exec(callback);
}

// Push
module.exports.pushVoterToArticle = function(ids, callback) {
    Article.findByIdAndUpdate(ids.articleId, {$push: {upvotes: ids.userId}}, {safe: true, upsert: true}, callback);
}
module.exports.pushDownvoterToArticle = function(ids, callback) {
    Article.findByIdAndUpdate(ids.articleId, {$push: {downvotes: ids.userId}}, {safe: true, upsert: true}, callback);
}

// Pop
module.exports.popVoterFromArticle = function(ids, callback) {
    Article.findByIdAndUpdate(ids.articleId, {$pull: {upvotes: ids.userId}}, {safe: true, upsert: true}, callback);
}
module.exports.popDownvoterFromArticle = function(ids, callback) {
    Article.findByIdAndUpdate(ids.articleId, {$pull: {downvotes: ids.userId}}, {safe: true, upsert: true}, callback);
}


module.exports.findIfUserUpvotedArticle = function(ids, callback){
    Article.find({$and: [{_id: ids.articleId}, {upvotes: [ids.userId]}]}, callback);
}

module.exports.findIfUserDownvotedArticle = function(ids, callback){
    Article.find({$and: [{_id: ids.articleId}, {downvotes: [ids.userId]}]}, callback);
}

module.exports.insertComment = function(data, callback){
    Article.findByIdAndUpdate(data.articleId, {$push: {comments: data.comment}}, {safe: true, upsert: true}, callback);
}

module.exports.findArticleById = function(articleId, callback){
    Article.find({_id: articleId}, callback);
}

module.exports.filterArticlesByTag = function(tagss, limit, skip, callback){
    Article.find({tags: {$all: tagss}}).skip(Number(skip)).limit(Number(limit)).exec(callback);
}