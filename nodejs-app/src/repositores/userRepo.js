const User = require('../models/userModel');

module.exports.findById = function(id, callback) {
    User.findById(id, callback);
}

module.exports.findOne = function(query, callback) {
    User.findOne(query, callback);
}

module.exports.save = function(newUser, callback){
    newUser.save(callback);
}

module.exports.findUserById = function(userId, callback) {
    User.findUserById(userId, {_id: 0, firstName: 1, lastName: 1, username: 1}, callback);
}