const mongoose = require('mongoose');

// User schema
const TagSchema = mongoose.Schema({
    name: { type: String }
});

var Tag = mongoose.model('Tag', TagSchema);
module.exports = Tag;