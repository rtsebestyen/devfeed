const mongoose = require('mongoose');

// User schema
const ArticleSchema = mongoose.Schema({
    authorId: { type: String, required: true },
    authorName: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true},
    link: {type: String, required: true},
    date: {type: Date, required: true},
    upvotes: [String],
    downvotes: [String],
    comments: [{
        comment: {type: String, required: true},
        userId: {type: String, required: true},
        date: {type: Date, required: true},
        username: {type: String, required: true},
    }],
    tags: [String]
});

var Article = mongoose.model('Article', ArticleSchema);
module.exports = Article;