const articleRepo = require('../repositores/articleRepo');
const tagRepo = require('../repositores/tagRepo');


module.exports.insertArticle = function(newArticle, callback) {
    articleRepo.save(newArticle, callback);
}

module.exports.findArticleByAuthorId = function(id, callback) {
    articleRepo.findArticleByAuthorId(id, callback);
}

module.exports.removeArticleById = function(id, callback){
    articleRepo.removeArticleById(id, callback);
}

module.exports.editArticleById = function(article, callback){
    articleRepo.editArticleById(article, callback);
}

module.exports.findAllArticles = function(limit, page, callback){
    const thispage = Number(page); 
    let skip = -1;
    if (thispage === 1){
        skip = 0
    } else {
        skip = limit * (page - 1);
    }
    articleRepo.findAllArticles(limit, skip, callback);
}

// Push
module.exports.pushVoterToArticle = function(ids, callback){
    articleRepo.pushVoterToArticle(ids, callback);
}
module.exports.pushDownvoterToArticle = function(ids, callback){
    articleRepo.pushDownvoterToArticle(ids, callback);
}

// Pop
module.exports.popVoterFromArticle = function(ids, callback){
    articleRepo.popVoterFromArticle(ids, callback);
}
module.exports.popDownvoterFromArticle = function(ids, callback){
    articleRepo.popDownvoterFromArticle(ids, callback);
}

module.exports.findIfUserUpvotedArticle = function(ids, callback){
    articleRepo.findIfUserUpvotedArticle(ids, callback);
}

module.exports.findIfUserDownvotedArticle = function(ids, callback){
    articleRepo.findIfUserDownvotedArticle(ids, callback);
}

module.exports.insertComment = function(data, callback){
    articleRepo.insertComment(data, callback);
}

module.exports.findArticleById = function(articleId, callback){
    articleRepo.findArticleById(articleId, callback);
}

module.exports.findAllTags = function(callback){
    tagRepo.findAllTags(callback);
}

module.exports.insertTag = function(newTag, callback){
    tagRepo.insertTag(newTag, callback);
}

module.exports.filterArticlesByTag = function(tags, limit, page, callback){
    const thispage = Number(page);
    let skip = -1;
    if (thispage === 1){
        skip = 0
    } else {
        skip = limit * (page - 1);
    }
    articleRepo.filterArticlesByTag(tags, limit, skip, callback);
}
