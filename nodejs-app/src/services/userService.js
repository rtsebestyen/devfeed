const bcrypt = require('bcryptjs');

const userRepo = require('../repositores/userRepo');

module.exports.getUserbyId = function(id, callback) {
    userRepo.findById(id, callback);
}

module.exports.findUserByUsername = function(username, callback){
    userRepo.findOne({username: username}, callback);
}

module.exports.insertUser = function(newUser, callback){
    newUser.password = bcrypt.hashSync(newUser.password, bcrypt.genSaltSync(10));
    userRepo.save(newUser, callback);
}

module.exports.comparePassword = function(password1, hashedPassword, callback){
    bcrypt.compare(password1, hashedPassword, (err, res) => {
        if (err)
            throw err;
        callback(null, res); 
    });
}

module.exports.findUserById = function(userId, callback){
    userRepo.findUserById(userId, callback);
}
