const express     = require('express'),
    path          = require('path'),
    bodyParser    = require('body-parser'),
    cors          = require('cors'),
    passport      = require('passport');

const users       = require('./routes/userRouter'),
    articles      = require('./routes/articleRouter'),
    connection    = require('./config/connection'),
    config        = require('./config/configurations');

const app = express();

// Database setup
connection.databaseSetup();

app.use(cors());

app.use(bodyParser.json());

// Routers
app.use('/users', users);
app.use('/articles', articles);

app.use(passport.initialize());
// app.use(passport.session());
require('./config/passport')(passport);

// Set static directory
app.use(express.static(path.join(__dirname, '../public')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'));
});

// Start server
app.listen(config.port, () => {
    console.log('Server listening on port ' + config.port);
});
